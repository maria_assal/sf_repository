public class MyExtension {
    private final Account acct;
    private String employees;
    public MyExtension(ApexPages.StandardController controller) {
        this.acct = (Account)controller.getRecord();
    }
    public String getTitle() {
        return 'Account: ' + acct.name + ' (' + acct.id + ')';
    }
    public String getInfo() {
        return 'Account query: '+acct;
    }
}