global class FacebookURLRewriter implements Site.UrlRewriter {
    String CONTACT_VISUALFORCE_PAGE = '/FacebookRedirect?contId=';
    
    global PageReference mapRequestUrl(PageReference fbAppUrl){
       system.debug('fbAppUrl == '+fbAppUrl);
         String url = fbAppUrl.getUrl();
        
             String oauth_code  = fbAppUrl.getParameters().get('code');
             String contId =  fbAppUrl.getParameters().get('contId');
             system.debug('code url '+oauth_code);
             system.debug('contact id '+contId );
             PageReference p=new PageReference('www.google.com');
             p.setRedirect(true);
             system.debug(p);
             return p;
             
    }
    
    global PageReference[] generateUrlFor(PageReference[] mySalesforceUrls){
         List<PageReference> myFriendlyUrls = new List<PageReference>();
         system.debug('mySalesforceUrls== '+mySalesforceUrls);
          for(PageReference mySalesforceUrl : mySalesforceUrls){
               if(mySalesforceUrl.getUrl().startsWith('/FacebookRedirect')){
                 String oauth_code  = mySalesforceUrl.getParameters().get('code');
                 String contId =  mySalesforceUrl.getParameters().get('contId');
                 system.debug('code url '+oauth_code);
                 system.debug('contact id '+contId );
                
                myFriendlyUrls.add(new PageReference(CONTACT_VISUALFORCE_PAGE  + contId ));
                }
          }
         return myFriendlyUrls;
    }
}