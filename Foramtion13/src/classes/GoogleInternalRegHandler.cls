global class GoogleInternalRegHandler implements Auth.RegistrationHandler{
    global User createUser(Id portalId, Auth.UserData data){
        User u = [SELECT Id FROM user WHERE GoogleID__c =: data.identifier];
        return u;
    }
    
    global void updateUser(Id userId, Id portalId, Auth.UserData data){
    }
}