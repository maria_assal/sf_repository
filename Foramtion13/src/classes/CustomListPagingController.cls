public with sharing class CustomListPagingController {

    public Integer pageSize{get;set;}
    public Integer getPageSize(){return pageSize;}
    public Integer height =  pageSize!= null? 24*(pageSize+1): 240;
    public Integer getHeight(){return height;}
    public String queryObject{get;set;}
    public Set<String> setFieldsName;
    public List<String> fieldsName{get;set;}
    public map<String, string> objectFielsMap{get;set;}
    public LIst<sobject> lstData{get;set;}
    public String whereQuery{get;set;}
    public String titleName{get;set;}
    public integer TotalRecord{ get{return  con.getresultSize();}}
    public Integer calculationPaging
      {
 
        get 
        {
        Integer retVal= 0;
        if(con.getresultSize() <con.getPageSize())
        {
         retVal=con.getresultSize();
        }
        else
        {

          if(con.getPageNumber()* con.getpageSize() <con.getresultSize())
          {
            retVal=con.getPageNumber()* con.getpageSize() ;
          }
          else
          {
           retVal=con.getresultSize();
          }
        }
        
        
        return retVal;
        }
        set;
 
    }
 
    private String sortDirection = 'ASC';
    private String sortExp = 'Name';
    public String sortExpression {
      get
      {
         return sortExp;
      }
      set
      {
         //if the column is clicked on then switch between Ascending and Descending modes
         if (value == sortExp)
           sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
         else
            sortDirection = 'ASC';
         sortExp = value;
      }
    }
    
    public String getSortDirection(){
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
          return 'ASC';
        else
         return sortDirection;
     }
    
     public void setSortDirection(String value){  
        sortDirection = value;
     }
     
     public List<SObject> getRecords(){
       if(con != null){  
           lstdata = new LIst<Sobject>();
           lstData= con.getRecords();
            height = 24*(lstData.size()+1);
            return lstData;  
        }
        else  
            return null ;  
     }
        
    // instantiate the StandardSetController from a query locator
    public ApexPages.StandardSetController con {
        get {
            if(con == null) {
                
                String selectfields = ComposeSelectQuery(fieldsName);
                RetrieveFieldsLabels(fieldsName);
                
                String query = 'Select Id, '+selectfields +' LastmodifiedDate FROM ' +queryObject;
                if (whereQuery != null && whereQuery !='')
                {
                     query +=' where '+whereQuery;
                }
               
                con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
                con.setPageSize(pageSize); 
            }
            return con;
        }
        set;
    } 
    
    public PageReference ViewData() {
        String selectfields = ComposeSelectQuery(fieldsName);
                
        String query = 'Select Id, '+selectfields +' LastmodifiedDate FROM ' +queryObject;
        string sortFullExp = sortExpression  + ' ' + sortDirection;
        if (whereQuery != null && whereQuery !='')
        {
             query +=' where '+whereQuery; 
        }
        query +=' order by ' + sortFullExp;
        
        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));     
        con.setPageSize(pageSize); 
        return null;
    }
    
    private void RetrieveFieldsLabels(List<String> fieldsName) 
    {
       objectFielsMap = new map<string,string>();
       Schema.sObjectType objType = Schema.getGlobalDescribe().get(queryObject); 
       Schema.DescribeSObjectResult r1 = objType.getDescribe(); 
       Map<String , Schema.SObjectField> mapFieldList = r1.fields.getMap();  
      
       for(String f : fieldsName)
       {
           if(mapFieldList.containskey(f))
           {
               objectFielsMap.put(f,mapFieldList.get(f).getDescribe().getLabel());
           }
          /* else
           {
               list<string> fieldName=f.split('\\.');
               string label=fieldName.get(fieldName.size()-1);
               objectFielsMap.put(f,label);
           }*/
       }
    }
    
    private String ComposeSelectQuery(list<String> fieldsName){
        String fields = '';
        set<string> fieldsSet= new set<string>(); 
        if ( fieldsName == Null)
        {
             fieldsName = new List<String>();
             fieldsName.add('Name');
        }
        else
        {
            for(String field : fieldsName)
            { 
                fieldsSet.add(field ); 
            } 
            if(!fieldsSet.contains('Name') && !fieldsSet.contains('name'))
            {
                fieldsName.add(0,'Name');
            }
        }   
        for (String field : fieldsName)
        {
            fields += field +', ' ;
        } 
   
        return fields;
    }
  
}