public class FbChartController {
    public Contact ct;
    public List<Facebook_Category_Like__c> listCategoryLikes;
    public FbChartController(ApexPages.StandardController controller) {
        Contact ctc = (Contact )controller.getRecord();
        ct=[Select id, FirstName, LastName, Facebook_Friends__c From Contact Where Id = :ctc.Id];
        listCategoryLikes = [Select Id, Name, Number_of_Likes__c, Likes_Pourcentage__c From Facebook_Category_Like__c Where contact__c= :ctc.Id];
        
    }

     public List<PieWedgeData> getPieData() {
        List<PieWedgeData> data = new List<PieWedgeData>();
        if(listCategoryLikes.size() > 0){
            Decimal total = 0;
            for(Facebook_Category_Like__c category : listCategoryLikes ){
                if(category.Likes_Pourcentage__c > 5.0)
                    data.add(new PieWedgeData(category.Name, category.Number_of_Likes__c));
                else
                    total += category.Number_of_Likes__c;
            }
             data.add(new PieWedgeData('Other',total));
        }
      
        return data;
    }

    public List<FriendsData> getFriendsData() {
        Contact mostPop = [SELECT id, firstname, lastname ,Facebook_Friends__c From contact
                                Where isAllowFB__c = true
                                Order bY Facebook_Friends__c Desc
                                Limit 1];
        Contact leastPop= [SELECT id, firstname, lastname ,Facebook_Friends__c From contact
                                Where isAllowFB__c = true
                                Order bY Facebook_Friends__c ASC
                                Limit 1];
        List<FriendsData> data = new List<FriendsData>();
        if(mostPop.id == ct.id){
            data.add(new FriendsData('Most Popular: '+ct.FirstName+' '+ct.LastName, ct.Facebook_Friends__c ));
            data.add(new FriendsData('Least Popular', leastPop.Facebook_Friends__c));  
         }
        else if (leastPop.Id == ct.Id ){
                 data.add(new FriendsData('Most Popular', mostPop.Facebook_Friends__c ));
                 data.add(new FriendsData('Least Popular: '+ct.FirstName+' '+ct.LastName, ct.Facebook_Friends__c));
          }
          else{   
              data.add(new FriendsData('Most Popular', mostPop.Facebook_Friends__c ));    
              data.add(new FriendsData(ct.FirstName+' '+ct.LastName ,ct.Facebook_Friends__c));
              data.add(new FriendsData('Least Popular', leastPop.Facebook_Friends__c));      
             
            
         }
       
        return data;
    }
    
    // Wrapper class
    public class PieWedgeData {

        public String name { get; set; }
        public Decimal data { get; set; }

        public PieWedgeData(String name, Decimal  data) {
            this.name = name;
            this.data = data;
        }
    }
    
     // Wrapper class
    public class FriendsData{

        public String name { get; set; }
        public Decimal data { get; set; }

        public FriendsData(String name, Decimal  data) {
            this.name = name;
            this.data = data;
        }
    }


}