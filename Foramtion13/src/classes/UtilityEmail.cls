public class UtilityEmail{
 public static void sendEmail(List<EmailMessageWrapper> listEmailMessageWrapper, String emailTemplateName, list<String> stringsToRemove) {
     List<Messaging.SendEmailResult> listEmailResult = null;
     List<Messaging.Singleemailmessage> listSingleEmailMessages = new List<Messaging.Singleemailmessage>();
      EmailTemplate emailTemplate = [SELECT Id, Subject, HtmlValue, Body FROM EmailTemplate WHERE Name = :emailTemplateName];
     for (EmailMessageWrapper emailMessageWrapper : listEmailMessageWrapper) {
         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
         mail.setReplyTo('support@noreply.apec.fr');
         if(emailMessageWrapper.FromAddress != null && emailMessageWrapper.FromAddress.length() > 0)
             mail.setReplyTo(emailMessageWrapper.FromAddress);
         if(emailMessageWrapper.ToAddress != null && emailMessageWrapper.ToAddress.length() > 0)
             mail.setToAddresses(new String[] { emailMessageWrapper.ToAddress });
         else
             mail.setTargetObjectId(emailMessageWrapper.ToAddressId);
         if(emailMessageWrapper.BccAddress != null && emailMessageWrapper.BccAddress.length() > 0)
             mail.setBccAddresses(new String[] {emailMessageWrapper.BccAddress });
         if(emailMessageWrapper.FromDisplayName != null && emailMessageWrapper.FromDisplayName.length() >0)
             mail.setSenderDisplayName(emailMessageWrapper.FromDisplayName);
         String subject = null;
         if(emailMessageWrapper.Subject != null && emailMessageWrapper.Subject.length() > 0) {
             mail.setSubject(emailMessageWrapper.Subject);
             subject = emailMessageWrapper.Subject;
         }
         else
             subject = emailTemplate.Subject;

         for(String key: emailMessageWrapper.ParameterSubjectMap.keySet())
             subject = subject.replace(key, (emailMessageWrapper.ParameterSubjectMap.get(key) == null ? '' : emailMessageWrapper.ParameterSubjectMap.get(key)));

         mail.setSubject(subject);
         String htmlBody = emailTemplate.HtmlValue;
         String plainBody = emailTemplate.Body;
         for (String key : emailMessageWrapper.ParameterBodyMap.keySet()) {
             htmlBody = htmlBody.replace(key, (emailMessageWrapper.ParameterBodyMap.get(key) == null) ? '' : emailMessageWrapper.ParameterBodyMap.get(key));
             plainBody = plainBody.replace(key, (emailMessageWrapper.ParameterBodyMap.get(key) == null) ? '' : emailMessageWrapper.ParameterBodyMap.get(key));
         }
        /* if(stringsToRemove.size()>0){
             for(String s : stringsToRemove)
                 htmlBody =htmlBody.replace(s,'');
         }    */   
         mail.setHtmlBody(htmlBody);
         mail.setSaveAsActivity(false);
         mail.setPlainTextBody(plainBody);
         mail.setUseSignature(true);
         listSingleEmailMessages.add(mail);
     }
     if(!Test.isRunningTest())
         listEmailResult = Messaging.sendEmail(listSingleEmailMessages);
 }

}