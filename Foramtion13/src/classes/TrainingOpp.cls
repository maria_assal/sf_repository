public with sharing class TrainingOpp {

	public static void TrainingAcc () {
		List<Account> acclist = [Select id, name, type, (Select id, firstName, lastName From Contacts) 
								From Account];
						
		list<Opportunity> listOpp_toInsert = new List<Opportunity>();
		map<String, List<Contact>> mapAccContact = new map<String, List<Contact>>();
				
		for(Account myAccount : acclist)
		{
			mapAccContact.put(myAccount.Id, myAccount.Contacts);
			if(myAccount.type.contains('Customer')){
				//create opp
				listOpp_toInsert.add( CreateOpp(  myAccount.Id, 'Qualification'));
			}
			else {
				listOpp_toInsert.add( CreateOpp(  myAccount.Id, 'Close'));
				
			}
		}
		
		insert listOpp_toInsert;
		
		List<OpportunityContactRole> list_toInsert_OppCR = new List<OpportunityContactRole>();
		for (Opportunity opp : listOpp_toInsert)
		{
			for (Contact ct : mapAccContact.get(opp.AccountId))
			{
				list_toInsert_OppCR.add(CreateOppCR(opp.Id, ct.Id));
			}
		}
		
		insert list_toInsert_OppCR;
	}
	
	public static Opportunity CreateOpp (String AccId, string status){
		Opportunity opp = new Opportunity();
		opp.AccountId = AccId;
		opp.StageName=status;
		opp.closeDate=system.today();
		return opp;
	
	}
	public static OpportunityContactRole CreateOppCR (String opp, String ctID){
		OpportunityContactRole oppcr = new OpportunityContactRole();
		oppcr.ContactId = ctID;
		oppcr.OpportunityId = opp;
		oppcr.Role='';
		return oppcr;
	
	}
}