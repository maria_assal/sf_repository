public class AnalyticsCtrl {
    public PageReference RunPost() {
       
        Reports.ReportResults res = Reports.ReportManager.runReport(report );
        FeedItem[] posts = new FeedItem[res.getgroupingsDown().getgroupings().size()];
        for(Integer i=0;i<posts.size();i++) {
            FeedItem post = new FeedItem();
            post.ParentId = UserInfo.getUserId();
            post.Body = chatterMsg + res.getreportmetadata().getname() +'(' + res.getgroupingsDown().getgroupings()[i].getlabel()
            + ':'+ res.getfactmap().get(''+i+'!T').getaggregates()[0].getlabel()+')';
            posts[i] = post;
         }
        insert posts;
        return null;
    }


    public String chatterMsg { get; set; }

    public String report { get; set; }
}