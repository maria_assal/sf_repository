public class FacebookFriends
{
    public List<Datum> data { get; set; }
    public Paging paging { get; set; }
    
    public class Datum
    {
        public string name { get; set; }
        public string id { get; set; }
        public string birthday {get; set;}
        public Location location{get; set;}
    }
    
    public class Location 
    {
        public string name { get; set; }
        public string id { get; set; }
    }
    public class Paging
    {
        public string next { get; set; }
    }
}