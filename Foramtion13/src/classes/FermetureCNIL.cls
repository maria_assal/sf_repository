public Class FermetureCNIL {
    public static void processSendEmail(){
         List<EmailMessageWrapper> listEmailMessageWrapper = new List<EmailMessageWrapper>();
    
        List<Account> listAccount = [SELECT Id, Name,
                            (Select Id, CaseNumber, CreatedDate, OwnerId, Status FROM Cases )
                            From Account where Id='001b000000Eg4ll'];
        Map<String, String> mapBodyParams = new Map<String, String>();
        String table = '';
        for (Account a : listAccount ){
           mapBodyParams.put('{!Account.Name}', a.Name);
            List<Case> listCases= a.Cases;
            
            if(listCases.size()>0){
                for(Case c: listCases){
                    table += '<li>Case Number: '+c.CaseNumber+'<ul><li>Case status: '+c.Status+'</li>'+
                                                                  ' <li> Case owner: '+c.OwnerId+'</li>'+
                                                                '</ul></li>';
                }
            }
         }
        // table = table + '</table>';
         mapBodyParams.put('{!listCases}',table);
         
        Map<String, String> mapSubjectParams = new Map<String, String>();
        listEmailMessageWrapper.add(new EmailMessageWrapper('admin@salesforce.com','APEC Support', 'maria.assal@ei-technologies.com', null, 'Fermeture CNIL', mapSubjectParams, mapBodyParams));
        
        list<String> stringsToRemove = new List<String>{'<![CDATA[',']]>'};
    
       if(listEmailMessageWrapper.size() > 0)
           UtilityEmail.sendEmail(listEmailMessageWrapper, 'APEC Template',stringsToRemove );
    }
}