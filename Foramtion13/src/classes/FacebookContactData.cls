public class FacebookContactData{
    
    public static void getContactPermission(list<Contact> listContacts){
        OAuth_Service__c authService = [select Id,Name,AccessTokenUrl__c,AuthUrl__c,CallBackURL__c,ConsumerKey__c,
                                     ConsumerSecretKey__c,ReqTokenUrl__c from OAuth_Service__c where Name = 'Facebook' LIMIT 1];
        
        Messaging.Email[] listEmailsToSend=new Messaging.Email[]{};
        for(Contact cont: listContacts){
       
            string reqURL=getAuthCode(cont, authService);
            string body='<p>Dear '+cont.Salutation+' '+cont.FirstName+' '+cont.LastName+',</p><br/>';
            body+='<p>Our Salesforce Application would like your permission to access your Facebook information, specifically your Facebook e-mail address, your interests, your friends\' list and your page likes.</p>';
            body+='<p>Rest assured, we will not have access to any of your friends\' information, aside from their Facebook name, without their consent.</p>';
            body+='<p>The purpose of this Salesforce custom feature is to form an idea about a contact\'s social influence and activities.</p>';
            body+='<div style="text-align: center;"><a href="'+reqURL+'"><span style="text-align: center;"><b>I AGREE to give Salesforce access to my Facebook data.</b></span></a></div><br />';
         //   body+='<div style="text-align: center;"><a href="'+authService.CallBackURL__c+'?allow=false&contId='+cont.Id+'"><span style="text-align: center;"><b>I DO NOT AGREE to give Salesforce access to my Facebook data.</b></span></a></div><br />';
            Messaging.SingleEmailMessage mailToSend=ConstructEmail('Salesforce Application', new list<string>{cont.email}, new list<string>(), 'Salesforce would like to access your Facebook data', body);
            if(mailToSend!=NULL){
                listEmailsToSend.add(mailToSend);
            }
       
        }
        if(listEmailsToSend.size()>0){
            Messaging.sendEmail(listEmailsToSend);
        }
    }
    
    public static Messaging.SingleEmailMessage ConstructEmail(String FromDisplayName, String[] toAddresses, String[] bccAddresses, string subject, string body){
        if(toAddresses.size() > 0 && toAddresses.get(0)!=null){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();        
            mail.setToAddresses(toAddresses);
            if(bccAddresses != null && bccAddresses.size() > 0){
              mail.setBccAddresses(bccAddresses);
            }
            //mail.setReplyTo('noreply@facebook.app');
            mail.setSenderDisplayName(FromDisplayName);
            mail.setSubject(subject);        
            mail.setHtmlBody(body);
            System.debug('### mail : ' + mail);
            //Messaging.sendEmail(new Messaging.Email[] { mail });
            return mail;
        }
        return NULL;      
    }
    
    //This method creates and returns a URL to get a temporary token
    public static String getAuthCode(Contact cont, OAuth_Service__c authServ){
        string retUrl='';
        String state = getRandomState(); //Random String
        String body='client_id='+authServ.ConsumerKey__c+'&state='+state;
        body=body+'&scope=email,user_interests,user_likes,user_birthday,user_friends,friends_birthday,friends_location'; // add scope access to the request
        body=body+'&redirect_uri='+authServ.CallBackURL__c+'?contId='+cont.id; // redirect to the public site
        retUrl=authServ.ReqTokenUrl__c+'?'+body;
        system.debug(retUrl);
   
        return retUrl;
    }
    
    //Generates Nonce Randomly
    public static String getRandomState(){
        String allChars='ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        String nonce='';
        for(integer cnt=0; cnt<=9;cnt++)   {
            Integer i = 1+Math.Round(700.0*Math.random()/26);
            if (i<=26)
            {
                i--;
                String newStr=allChars.substring(i,i+1);
                nonce=nonce+newStr;
            }else{
                cnt--;
            }
        }
        return nonce;
    } 
}