public class BarcodeGeneratorCtrl {

    public String codeTxt { get; set; }
    public static String response{get; set;}
    public static boolean isdone {get;set;}
    
    public PageReference Generate() {
        system.debug(codeTxt);
        CallCodeGenerator(codeTxt);
        return null;
    }

    @future(callout=true)
    public static void CallCodeGenerator(String txt ){
      //  String AppSID='0bc41258-2ece-452c-97e7-324ebdd584f2';
     //   String Appkey='75c82e8f3110d77f13ec1d76c10726e1';
        
         HttpRequest req = new HttpRequest();
         String url = 'http://www.barcodesinc.com/generator/image.php?code='+txt+'&style=325&type=C128B&width=200&height=50&xres=1&font=3';
                req.setEndPoint(url );         
        req.setMethod('GET');
      req.setHeader('Content-Type', 'application/xml');
     
        req.setTimeout(60000);
        Http http = new Http();
        
        HttpResponse res = http.send(req);
       system.debug( res.getBody());
       response=res.getBody();
       isdone=true;
    
    }
}