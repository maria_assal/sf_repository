public with sharing class SortListOfObjectClass {

	public static void testSort(){
		List<ProspectAltares> proplist = new list<ProspectAltares>();
		map<string,list<ProspectAltares>> stracc = new map<string,list<ProspectAltares>>();
		
		ProspectAltares p1 = new ProspectAltares();
		p1.Nom = 'maria';
		p1.Adresse='liban';
		ProspectAltares p2 = new ProspectAltares();
		p2.Nom = 'adele';
		p2.Adresse='usa';
		proplist.add(p1);
		proplist.add(p2);
		
		for(ProspectAltares p : proplist){
		    if(stracc.containskey(p.Nom)){
		        stracc.get(p.Nom).add(p);
		    }else
		        stracc.put(p.Nom, new ProspectAltares[]{p});
		}
		proplist = new list<ProspectAltares>();
		list<String> namelist = new list<string>();
		namelist.addAll(stracc.keyset());
		namelist.sort();
		for (String s : namelist){
		    proplist.addAll(stracc.get(s));
		}
	}
	public class ProspectAltares
	{
	    public String Nom {get;set;}
	    public String Adresse {get;set;}
	}
}