public class FacebookLikes{

    public List<Datum> data { get; set; }
    public Paging paging { get; set; }
    
    public class Datum
    {
        public string category{ get; set; }
        public list<CategoryList> category_list{ get; set; }
        public string name{ get; set; }
        public string created_time{ get; set; }
        public string id { get; set; }
    }
    
    public class CategoryList
    {
        public string id { get; set; }
        public string name { get; set; }
    }
    
    public class Paging
    {
        public Cursor cursors {get; set; }
        public string next { get; set; }
    }

    public class Cursor
    {
        public string after{ get; set; }
        public string before{ get; set; }
    }
}