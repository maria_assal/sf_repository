public class FbStatController {

    public String contact2Id { get; set; }
    public String contact1Name { get; set; }
    
    public String contact1Id { get; set; }
    public String contact2Name { get; set; }
    public static map<String,List<Decimal>> mapCategories;
    public Contact popularCtc { 
        get{
            popularCtc = [SELECT id, firstname, lastname ,Facebook_Popularity__c, Facebook_Friends__c From contact
                                Where isAllowFB__c = true
                                Order bY Facebook_Friends__c Desc
                                Limit 1];
            return popularCtc ;
        } 
        set; 
    }
     public Contact unpopularCtc { 
        get{
            unpopularCtc = [SELECT id, firstname, lastname ,Facebook_Popularity__c, Facebook_Friends__c From contact
                                Where isAllowFB__c = true
                                Order bY Facebook_Friends__c ASC
                                Limit 1];
            return unpopularCtc ;
        } 
        set; 
    }
    public FbStatController() {
       
    }
   
    public PageReference CompareLikes() {
        PageReference retVal;
        mapCategories = new  map<String,List<Decimal>>();
        List<Facebook_Category_Like__c> listFBlikesCont1 = [Select Id, name, Number_of_Likes__c, Likes_Pourcentage__c From Facebook_Category_Like__c Where contact__c = :contact1Id AND Likes_Pourcentage__c>= 1.0];
        for(Facebook_Category_Like__c catLike : listFBlikesCont1){
            mapCategories.put(catLike.name, new List<Decimal>{catLike.Likes_Pourcentage__c,0});
          
        }
        List<Facebook_Category_Like__c> listFBlikesCont2 = [Select Id, name, Number_of_Likes__c, Likes_Pourcentage__c From Facebook_Category_Like__c Where contact__c = :contact2Id AND Likes_Pourcentage__c>= 1.0]; 
        for(Facebook_Category_Like__c catLike : listFBlikesCont2){
             list<Decimal> newlist = new list<Decimal>();
          
           if(mapCategories.containsKey(catLike.name)){
               newlist = mapCategories.get(catLike.name);
               newlist.remove(1);
               newlist.add(catLike.Likes_Pourcentage__c);
               mapCategories.put(catLike.name, newlist);
           }
           else{
               newlist.add(0);
                newlist.add(catLike.Likes_Pourcentage__c);
                mapCategories.put(catLike.name, newlist);
           }
        }   
        return retVal;
    }

    PageReference openLookupPopup(){
        return null;
    }
    public List<Gauge> getGaugeData() {
       List<Gauge> data = new List<Gauge>();
       
         data.add(new Gauge('popularity',popularCtc.Facebook_Friends__c, unpopularCtc.Facebook_Friends__c));
       return data;   
    }
    
    public List<CatData> getCategoryData() {
       List<CatData> data = new List<CatData>();
       List<Contact> ctlist = [Select Id, (Select Id, name, Number_of_Likes__c From Facebook_Category_Likes__r order by Number_of_Likes__c  DESC) From Contact Where isAllowFB__c=true];
       map<String, Decimal> categorieLikes = new map<String, Decimal>();
       for (Contact c : ctlist){
           List<Facebook_Category_Like__c> fbCat = c.Facebook_Category_Likes__r;
           for(Facebook_Category_Like__c cat : fbCat ){
               decimal total=0;
               if(categorieLikes.containsKey(cat.name)){
                   total = categorieLikes.get(cat.name) + cat.Number_of_Likes__c ;
                   categorieLikes.put(cat.name, total);
               }
               else
                   categorieLikes.put(cat.name, cat.Number_of_Likes__c);
           }
       }
        List<Decimal> totals=  categorieLikes.values();
        totals.sort();
        Decimal maxTotal = totals.get(totals.size()-1);
       system.debug('categorieLikes data = '+maxTotal );
       for(String name : categorieLikes.keyset()){
           if(categorieLikes.get(name) >= maxTotal/2 )
                data.add(new CatData(name,categorieLikes.get(name)));
       }          
       return data;   
    }
      
    
     public static List<Data> getData() {
         List<Data> data = new List<Data>();
         if(mapCategories != null){
            for(String catName : mapCategories.keyset()){
               data.add(new Data(catName,  mapCategories.get(catName).get(1),mapCategories.get(catName).get(0)));
            }
            
        }
        return data;
    }
    
    // Wrapper classes 
    public class Data {
        public String name { get; set; }
        public Decimal data1 { get; set; }
        public Decimal data2{ get; set; }
        public Data(String name, Decimal data1, Decimal data2) {
            this.name = name;
            this.data1 = data1;
             this.data2 = data2;
           
        }
    }
    public class CatData{
        public String name { get; set; }
        public Decimal data1 { get; set; }
        public CatData(String name, Decimal data1) {
            this.name = name;
            this.data1 = data1;
        }
    }
    public class Gauge{
         public string name{ get; set; }
        public Decimal popular { get; set; }
         public Decimal unpopular { get; set; }
        public Gauge(string name , Decimal popular, Decimal unpopular ) {
            this.name = name ;
             this.popular = popular ;
           this.unpopular = unpopular ;
        }
    }
    
}