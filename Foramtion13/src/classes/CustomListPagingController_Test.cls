@isTest
private class CustomListPagingController_Test{
    static testMethod void PagingComponentTest() {
        test.startTest();
        CustomListPagingController gridcontroler= new CustomListPagingController();
        gridcontroler.queryObject='Account';
        list<String> fields = new list<String>();
        fields.add('Phone');
        gridcontroler.fieldsName=fields ;
        gridcontroler.pageSize=6;
        gridcontroler.sortExpression='DESC';
        String sorting = gridcontroler.getSortDirection();
        ApexPages.StandardSetController con =gridcontroler.con;
        gridcontroler.ViewData();
        gridcontroler.getRecords();
        
        test.stopTest();
    }

}