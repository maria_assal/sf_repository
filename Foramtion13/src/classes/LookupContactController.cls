public class LookupContactController {

    public list<Contact> contacts { get; set; }
    public String query {get; set;}
    
    public PageReference runQuery()
    {
        if(query  != '' ){
            List<List<Contact>> searchResults=[FIND :query IN ALL FIELDS RETURNING Contact (id, firstname, lastName)];
            contacts =searchResults[0];
        }
        return null;
    }
}