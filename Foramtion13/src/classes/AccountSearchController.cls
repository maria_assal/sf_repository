public class AccountSearchController {

    public String paysText { get; set; }

    public String villeText { get; set; }

    public String addressText { get; set; }
 
    public String nameText;
    List<Account> results;
    
    public String getNameText() {
        return nameText;
    }

    public void setNameText(String name){
        nameText=name;
    }  
    public List<Account> getResults() {
        return results;
    }

    public PageReference doSearch() {
        
        results = (List<Account>)[SELECT Name, AccountNumber, SIREN__c, Ville__c, Pays__c FROM Account 
                                        WHERE  Name= :nameText AND Ville__c= :villeText];
        return null;
    }

}