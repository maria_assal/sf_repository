public with sharing class ChartCtrl {

     public list<Data> getData() {
        list<Data> data = new list<Data>();
       data.add(new Data('2013',10.0,10, 21, 15));    
     data.add(new Data('2014',15.0,30, 25, 20));  
      data.add(new Data('2015',20.0,15, 33,30));  
       data.add(new Data('2016',21.0,25, 36,20));  
      return data;
    }
     // Wrapper class
    public class Data{

        public String name { get; set; }
        public Decimal data1 { get; set; }
         public Decimal data2 { get; set; }
          public Decimal data3 { get; set; }
        public Decimal data4 { get; set; }

        public Data(String name, Decimal  data1,Decimal  data2,  Decimal  data3, Decimal  data4) {
            this.name = name;
            this.data1 = data1;
             this.data2 = data2;
              this.data3 = data3;
               this.data4 = data4;
        }
    }
   

}