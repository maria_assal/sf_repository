public class Facebook_UpdateContact{
    public static boolean msgAllow{get;set;}
     public Facebook_UpdateContact(ApexPages.StandardController controller) {
       
    }
    
    public PageReference getAuthToken(){
        PAgeReference logoutPage;
        String oauth_code = ApexPages.currentPage().getParameters().get('code');
        String cont_id = ApexPages.currentPage().getParameters().get('contId');
       
        if (oauth_code != null ){
            // Getting authentication token
            OAuth_Service__c authServ = [select Id,Name,AccessTokenUrl__c,AuthUrl__c,CallBackURL__c,ConsumerKey__c,
                                         ConsumerSecretKey__c,ReqTokenUrl__c
                                         from OAuth_Service__c
                                         where Name = 'Facebook'
                                         LIMIT 1];
            String body='client_id='+authServ.ConsumerKey__c+'&client_secret='+authServ.ConsumerSecretKey__c+'&code='+oauth_code;
            body=body+'&redirect_uri='+authServ.CallBackURL__c+'?contId='+cont_id;
            String  authToken = httpCallout(authServ.AccessTokenUrl__c,'',body,'GET');
         
            System.debug('**********RESPONSE:'+authToken);
            
           if( !isAllowedFacebook( cont_id)){    
                String fbInfo = httpCallout('https://graph.facebook.com/me','',authToken,'GET');
                String fbFriends = httpCallout('https://graph.facebook.com/me/friends','',authToken+'&fields=id,name,birthday,location&limit=1000','GET');
                String fbLikes = httpCallout('https://graph.facebook.com/me/likes','',authToken+'&limit=500','GET');
                system.debug(fbFriends);
                UpdateContact(cont_id, authToken);
                AddFriends(cont_id, fbFriends);
                AnalyseFbLikes(cont_id, fbLikes);
               
            }
            
             /// loging out of facebook
            string encodedURL=EncodingUtil.URLENCODE(authServ.CallBackURL__c+'?logout=1','UTF-8');
            if( !msgAllow){
                encodedURL=EncodingUtil.URLENCODE(authServ.CallBackURL__c+'?logout=0','UTF-8');
            }
            logoutPage=new PageReference('https://www.facebook.com/logout.php?next='+encodedURL+'&'+authToken);
            logoutPage.setRedirect(true);
        }
        else if(ApexPages.currentPage().getParameters().get('logout') != null && ApexPages.currentPage().getParameters().get('logout')=='1'){
             msgAllow=true;
        }
        return logoutPage;
    }
    
    private static String httpCallout(String endPoint,String header, String body,String method){
        HttpRequest req = new HttpRequest();
        endpoint=endpoint+'?'+body;
        req.setEndpoint(endPoint);
        req.setMethod(method);
        Http http = new Http();
        HTTPResponse res = http.send(req);
        return res.getBody();
    }
    
    private static void UpdateContact(string contId, string token){
        contact c = new contact(id=contId, FBAuthToken__c=token, isAllowFB__c=true );
        update c;
    }
    
    private static void AddFriends(string contId, string data){
    
        FacebookFriends listFbFriends=(FacebookFriends)JSON.deserialize(data,FacebookFriends.class);
        list<FacebookFriends.Datum> ff = listFbFriends.data;
        system.debug( listFbFriends.paging.next);
        list<Facebook_Friend__c> listToUpsert=new list<Facebook_Friend__c>();
        for (FacebookFriends.Datum f: ff)
        {
        	Facebook_Friend__c fbFrnd = new Facebook_Friend__c();
        	fbFrnd.Facebook_ID__c = f.Id;
        	fbFrnd.Name = f.name;
        	fbFrnd.Contact__c = contId;
        	fbFrnd.Fb_Contact__c = f.Id+''+contId;
        	if (f.birthday != null && f.birthday != '')
        	{
        		fbFrnd.Tech_FbBirthday__c = f.birthday;
        		List<String> datelist = f.birthday.split('/');
				if(datelist.size() == 3){
				    Date myDate = Date.newInstance(Integer.valueOf(datelist[2]), Integer.valueOf(datelist[0]), Integer.valueOf(datelist[1]));
				    fbFrnd.Birthday__c = myDate;
				}
        	}
        	if(f.location != null){
            	fbFrnd.Address__c=f.location.name;
            }
            
            listToUpsert.add(fbFrnd);
        }
        if(listToUpsert.size()>0)
        {
            upsert listToUpsert;
        }
    }
    
    private static void AnalyseFbLikes(String contId, String data){
        FacebookLikes listFbLikes = (FacebookLikes) JSON.deserialize(data, FacebookLikes.class);
        
        map<String, Integer> mapCategoryLikes = new map<String, Integer>();
        Integer count=0;
        list<FacebookLikes.Datum> listData = listFbLikes.data;
        for(FacebookLikes.Datum likes : listData)
        {
            if( !mapCategoryLikes.containsKey(likes.category)){
                mapCategoryLikes.put(likes.category,0);
            }
             count = mapCategoryLikes.get(likes.category);
             count++;
             mapCategoryLikes.put(likes.category,count);
        }
       
        list<Facebook_Category_Like__c> listfbCatLikes = new  list<Facebook_Category_Like__c>();
        for(String categoryName : mapCategoryLikes.keyset())
        {
            string extenId= categoryName+''+contId;
            listfbCatLikes.add(new Facebook_Category_Like__c(Name=categoryName, Contact__c=contId, Number_of_Likes__c=mapCategoryLikes.get(categoryName), Category_Contact__c=extenId));
        }
        if(listfbCatLikes.size()>0){
            upsert listfbCatLikes;
        }
    }
    
    private static boolean isAllowedFacebook (String contactId){
        Contact ct = [Select Id, isAllowFB__c From contact where Id = : contactId ];
        if(ct.isAllowFB__c){
            msgAllow =false;
        }
        else{
            msgAllow =true;
        }
        return ct.isAllowFB__c;
    }
}