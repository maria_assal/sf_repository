public class PositionExtension {
    public Job_Application__c[] ja {get;set;}
    public PositionExtension(ApexPages.StandardController controller) {
        Position__c pos = (position__c) controller.getRecord();
        
        getRelatedJobApplications(pos);
    }
    
    private void getRelatedJobApplications(SObject pos){
        String id= pos.Id;
        ja = [Select id, name, status__c, stage__c, candidate__r.First_Name__c, candidate__r.Last_Name__c, candidate__r.Social_Security_Number__c From Job_Application__c where position__c =:id ];
    }

}