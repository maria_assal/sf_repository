public with sharing class HerokuConnector {
	public static void callHerokuApp(){
		Http h = new Http();

		HttpRequest req = new HttpRequest();
		req.setEndpoint('http://secure-fortress-6136.herokuapp.com/');
		req.setMethod('GET');
		
		HttpResponse res = h.send(req);
		res.getBody();
		system.debug('>>>>>>>>>>> '+res.getBody());
	}
}