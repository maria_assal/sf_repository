@isTest
public class TestDataFactory{
    public static Invoice_Statement__c createOneInvoiceStatement(Boolean withLineItem){
        Invoice_Statement__c inv = createInvoiceStatement();
        
        if(withLineItem == true){
            Marchandise__c m = createMerchandiseItem('Orange juice');
            AddLineItem(inv, m);
        }
        
        return inv;
    }
    
    private static Marchandise__c createMerchandiseItem(String merchName) {
        Marchandise__c m = new Marchandise__c(
            Name=merchName,
            Description__c='Fresh juice',
            Price__c=2,
            Total_Inventory__c=1000);
        insert m;
        return m;
    }
    private static Invoice_Statement__c createInvoiceStatement() {
        Invoice_Statement__c inv = new Invoice_Statement__c( Description__c='Test Invoice');
        insert inv;
        return inv;
    }
    private static Line_Item__c AddLineItem(Invoice_Statement__c inv, Marchandise__c m) {
        Line_Item__c lineItem = new Line_Item__c(
            Invoice_Statement__c = inv.Id,
            Marchandise__c = m.Id,
            Unit_Price__c = m.Price__c,
            Unit_Sold__c = (Double)(10*Math.random()+1));
        insert lineItem;
        return lineItem;
    }
}