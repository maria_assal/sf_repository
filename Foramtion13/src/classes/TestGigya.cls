public with sharing class TestGigya {

	public static void ConnectToGigya(){
		String API_KEY ='3_54yNPpNkU5jYXgqMlFumvcFD1qbvp4PfvwDV32PqogaWw7xlZIP-NupBjWa-W360';
		String SECRET ='LIEmWHGPeW5oQPyo/XGM2/e9VzNqKaCevi+jXQSct0k=';
		
		Long timeStamp = system.currentTimeMillis();
		Double nonce = Math.random() * timeStamp;
		
		String queryString='select UID, loginProvider, data.ExactTargetID, profile.firstName, profile.lastName, profile.email, profile.birthDay, profile.birthMonth, profile.birthYear, profile.gender, data.Title, data.Adresse_line_1, data.Adresse_line_2, data.Adresse_line_3, data.Post_code, profile.city, profile.country, data.Tel, data.GSM, data.Receive_push_notifications, data.Receive_newsletter_and_offers_by_email, data.I_am, data.Marital_Status, data.Fuel_Type, data.My_Vehicule_Type, data.My_Favourite_Station, data.First_Payment_Date, data.First_open_date, data.Last_open_date, profile.nickname, data.Last_Payment_Date, profile.state, profile.age, profile.photoURL, profile.thumbnailURL, profile.profileURL, profile.languages, profile.bio, profile.username, profile.favorites.interests.id, profile.favorites.interests.name, profile.favorites.interests.category, profile.favorites.activities.id, profile.favorites.activities.category, profile.favorites.activities.name, profile.favorites.books.id, profile.favorites.books.name, profile.favorites.books.category, profile.favorites.music.id, profile.favorites.music.name, profile.favorites.music.category, profile.favorites.movies.id, profile.favorites.movies.name, profile.favorites.movies.category, profile.favorites.television.id, profile.favorites.television.name, profile.favorites.television.category, profile.likes.id, profile.likes.name, profile.likes.category from accounts';
		String urlString = 'http://accounts.eu1.gigya.com/accounts.search';
		String params = 'apiKey=' +Encodingutil.urlEncode(API_KEY,'UTF-8') +'&nonce='+nonce+
		    			'&query='+Encodingutil.urlEncode(queryString,'UTF-8').replace('+','%20')+'&timestamp='+ timeStamp;
		    			
		String BaseString= 'GET&'+Encodingutil.urlEncode(urlString,'UTF-8')+'&'+Encodingutil.urlEncode(params,'UTF-8');
		String signature = constructSignature(BaseString, SECRET);		                
		
		String requestURL = 'http://accounts.eu1.gigya.com/accounts.search?apiKey=' +Encodingutil.urlEncode(API_KEY,'UTF-8') + '&nonce='+ nonce+
				                '&sig='+ Encodingutil.urlEncode(signature,'UTF-8')+'&timestamp='+ timeStamp+'&query='+Encodingutil.urlEncode(queryString,'UTF-8');
		
		HttpRequest req = new HttpRequest();
		req.SetEndpoint(requestURL);
		req.setMethod('GET');
		Http http = new Http();
		HttpResponse res = http.send(req);
		system.debug(res.getBody());

	}
	
	public static String constructSignature(string baseString, string secretKey)
	{
		//String baseString = timestamp + '_' + UID;                                  // Construct a "base string" for signing
	    Blob binaryBaseString =  Blob.valueOf(baseString);    // Convert the base string into a binary array
	    Blob binaryKey = EncodingUtil.base64Decode(secretKey);     // Convert secretKey from BASE64 to a binary array
	   Blob binarySignature = Crypto.generateMac('HMACSHA1', binaryBaseString, binaryKey);      // Use the HMAC-SHA1 algorithm to calculate the signature
	    //Blob binarySignature = Crypto.sign('RSA', binaryBaseString, binaryKey);                // Convert the signature to a BASE64
	    String signature = Encodingutil.base64Encode(binarySignature);
	    
	    return signature;
		
	}
}