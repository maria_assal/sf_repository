public class controllerforcomponent {
  public String objectname{get;set;}
   public Integer pageSize = 6;
    public Integer getPageSize(){return pageSize;}
    public Integer height = 24*(pageSize+1);
    public Integer getHeight(){return height;}
    
     public LIst<sobject> lstData{get;set;}

     public List<Sobject> getrecords(){
         lstdata = new LIst<Sobject>();
         lstdata = con.getRecords();
          height = 24*(lstdata .size()+1);
         if(lstData.size() != null && lstdata.size()>0)
             return lstdata;
         else
             return null;
     }
     
     public ApexPages.StandardSetController con {
        get {
            if(con == null) {
                
                String query = 'Select Id, Name FROM ' +objectname+ ' Order By Name limit 100';
                system.debug('query = ' +query);
                con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
                // sets the number of records in each page set
                
                con.setPageSize(pageSize); 
                
            }
            return con;
        }
        set;
    } 
        
}