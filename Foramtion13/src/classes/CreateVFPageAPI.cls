public with sharing class CreateVFPageAPI {
	public static void doCreateVF(){
		String salesforceHost = System.Url.getSalesforceBaseURL().toExternalForm();

		String url =  salesforceHost + '/services/data/v29.0/sobjects/ApexPage';
		
		HttpRequest req = new HttpRequest();
		
		req.setMethod('POST');
		req.setEndpoint(url);
		req.setHeader('Content-type', 'application/json');
		req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
		//for controllerType = >0 -- no controller
		//req.setBody('{"Name" : "TestPageFromRest","Markup" : "<apex:page>hello</apex:page>","ControllerType" : "0","MasterLabel":"TestPageFromRest","ApiVersion":"30.0"}');
		
		//for controllerType => 1 -- Standard controller + extensions        
		req.setBody('{"Name" : "TestPageFromRestCase","Markup" : "<apex:page standardController=\'case\' >hello</apex:page>","ControllerType" : "1","MasterLabel":"TestPageFromRestCase","ApiVersion":"29.0"}');
		
		//for controllerType => 3 --custom Controller
		//req.setBody('{"Name" : "TestPageFromRestCase1","Markup" : "<apex:page controller=\'displaycase\'>hello</apex:page>","ControllerType" : "3","MasterLabel":"TestPageFromRestCase1","ApiVersion":"29.0"}');            
		Http http = new Http();
		
		HTTPResponse res = http.send(req);
		System.debug(res.getBody());
	}
}